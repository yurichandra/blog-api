require_relative '../rails_helper'

RSpec.describe TagController, :type => :controller do
  describe 'GET #index' do
    it 'return all tags' do
      get :index

      expect(response).to have_http_status(:ok)
    end
  end
end