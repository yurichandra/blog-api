Rails.application.routes.draw do
  get 'index/index'

  post 'auth', to: 'auth#authenticate'
  get 'whoami', to: 'auth#whoami'

  get 'tags', to: 'tag#index'
  post 'tags', to: 'tag#create'

  get 'categories', to: 'category#index'
  
  # For article module
  get 'articles', to: 'article#index'
  get 'articles/:id', to: 'article#find'
  get 'articles/by-slug/:slug', to: 'article#find_by_slug'
  post 'articles', to: 'article#create'
  put 'articles/:id', to: 'article#update'
  delete 'articles/:id', to: 'article#delete'

  root 'index#index'
end
