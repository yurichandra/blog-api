class ArticleController < ApplicationController
  include Articles

  before_action :auth, only: %w[create update]

  wrap_parameters :articles, include: %i[
    title
    body
    is_published
    is_preview
    category_id
    tag_ids
    summary
  ]

  def index
    published_param = params[:is_published] unless params[:is_published].nil?

    articles = Article.order(created_at: :desc)

    unless published_param.nil?
      articles = articles.published(published_param)
    end

    render json: articles
  end

  def find
    article = Article.find(params[:id])

    render json: article
  end

  def find_by_slug
    article = Article.find_by!(slug: params[:slug])

    render json: article
  end
  
  def create
    article = {
      title: article_params[:title],
      body: article_params[:body],
      category_id: article_params[:category_id],
      cover_image: article_params[:cover_image],
      is_published: article_params[:is_published],
      tag_ids: article_params[:tag_ids],
      is_preview: article_params[:is_preview],
      summary: article_params[:summary],
      user_id: current_user.id
    }

    article = Articles::StoreArticle.new(article).perform

    if article
      render json: article, status: :created
    else
      render json: article.errors, status: :unprocessable_entity
    end
  end

  def update
    article = {
      title: article_params[:title],
      body: article_params[:body],
      category_id: article_params[:category_id],
      cover_image: article_params[:cover_image],
      is_published: article_params[:is_published],
      tag_ids: article_params[:tag_ids],
      user_id: current_user.id
    }

    article = Articles::UpdateArticle.new(params[:id], article).perform

    if article
      render json: article
    else
      render json: article.errors, status: :unprocessable_entity
    end
  end

  def delete
    article = Article.find(params[:id])
    article.destroy

    render status: :no_content
  end

  private

  def article_params
    params.require(:articles).permit(
      :title,
      :body,
      :is_published,
      :is_preview,
      :category_id,
      :summary,
      :tag_ids => []
    )
  end
end