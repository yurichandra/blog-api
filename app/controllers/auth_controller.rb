class AuthController < ApplicationController
  before_action :auth, only: %w[whoami]

  def authenticate
    user = User.find_by(email: params[:email])
    if user.nil?
      render status: 400, body: { message: 'bad_request' }.to_json and return
    end

    password = BCrypt::Password.new(user.password)
    if password != params[:password]
      render status: 400, body: { message: 'bad_request' }.to_json and return
    end

    token = AccessToken.create!({
      access_token: SecureRandom.hex(32),
      expired_at: DateTime.current + 2.hour,
      user: user
    })

    render status: :ok, body: {
      access_token: token.access_token,
      expired_at: token.expired_at
    }.to_json
  end

  def whoami
    user = current_user
    if user.nil?
      render status: 401, body: { message: 'unauthorized' }.to_json and return
    end

    render json: JSON.generate({
      email: user.email,
      fullName: user.personal.full_name
    })
  end
end