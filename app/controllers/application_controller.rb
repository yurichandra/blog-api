class ApplicationController < ActionController::API
  def auth
    header = request.headers['Authorization']
    
    if header.nil?
      render status: 401, body: { message: 'unauthorized' }.to_json and return
    end

    token = header[7..header.length - 1]

    if token.empty?
      render status: :bad_request, body: { message: 'bad request' }.to_json and return
    end

    token
  end

  def current_user
    return if request.headers['Authorization'].nil?
    
    token = AccessToken.find_by(access_token: auth)

    if token.nil?
      return
    end

    token.user
  end
end
