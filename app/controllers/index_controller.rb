require 'json'

class IndexController < ApplicationController
  def index
    render body: {
      name: 'blog-api',
      description: 'Personal blog written in Ruby on Rails'
  }.to_json
  end
end