class TagController < ApplicationController
  before_action :auth, only: %w[create]

  wrap_parameters :tags, include: %i[name]
  
  def index
    tags = Tag.all

    render json: tags
  end

  def create
    tag = Tag.create!(tag_params)

    render json: tag
  end

  private

  def tag_params
    params.require(:tags).permit(:name)
  end
end