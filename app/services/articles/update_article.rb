module Articles
  class UpdateArticle
    def initialize(id, params)
      @id = id
      @params = params
    end

    def perform
      article = Article.find(@id)

      article.update(
        title: @params[:title],
        body: @params[:body],
        slug: @params[:title].parameterize,
        duration: ArticleLib.calculate_duration(@params[:body]),
        cover_image: @params[:cover_image],
        is_published: @params[:is_published],
        category_id: @params[:category_id],
        user_id: @params[:user_id]
      )

      article.tags = Tag.where(id: @params[:tag_ids])
      article.save

      article
    end
  end
end