module Articles
  class StoreArticle
    def initialize(params)
      @params = params
    end

    def perform
      article = Article.create!(
        title: @params[:title],
        body: @params[:body],
        slug: @params[:title].parameterize,
        duration: ArticleLib.calculate_duration(@params[:body]),
        cover_image: @params[:cover_image],
        is_published: @params[:is_published],
        is_preview: @params[:is_preview],
        category_id: @params[:category_id],
        user_id: @params[:user_id],
        summary: @params[:summary]
      )

      article.tags = Tag.where(id: @params[:tag_ids])
      article.save

      article
    end
  end
end