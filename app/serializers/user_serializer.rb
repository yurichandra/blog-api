class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :personal

  def personal
    {
      full_name: self.object.personal.full_name
    }
  end
end
