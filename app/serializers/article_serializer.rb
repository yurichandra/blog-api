class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :title, :body, :summary, :slug, :cover_image, :duration, :is_published, :is_preview
  attributes :created_at, :updated_at

  has_many :tags
  has_one :user
  has_one :category
end
