class ArticleLib
  # At the moment, to calculate reading duration will be divided by 250
  WORD_PER_MINUTE = 250

  def self.calculate_duration(body)
    (body.split.size / WORD_PER_MINUTE).round
  end
end