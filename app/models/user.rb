class User < ApplicationRecord
  has_one :personal

  has_many :access_token

  has_many :article
end
