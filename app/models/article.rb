class Article < ApplicationRecord
  has_and_belongs_to_many :tags

  belongs_to :category
  belongs_to :user

  validates :body, presence: true
  validates :title, presence: true
  validates :category_id, presence: true
  validates :slug, presence: true
  validates :duration, presence: true
  validates :user_id, presence: true
  validates :summary, presence: true

  scope :published, -> (value) { where(is_published: value) }
end
