require 'bcrypt'

# =============== Seed Users ================================

users = [
  {
    email: 'yurichandra@gmail.com',
    password: BCrypt::Password.create(ENV['DEFAULT_PASSWORD']),
    personal: {
      full_name: 'Yuri Chandra Tri Putra',
      phone_number: '08123456789',
    }
  }
]

for user in users do
  found_user = User.find_by(email: user[:email])

  if found_user.nil?
    new_user = User.create({ email: user[:email], password: user[:password]})
    Personal.create({
      full_name: user[:personal][:full_name],
      phone_number: user[:personal][:phone_number],
      user: new_user
    })
  end
end

# =============== Seed Tags ================================

tags = %w[
  javascript
  ruby
  go
  database
  vue
]

for tag in tags do
  found_tag = Tag.find_by(name: tag)

  if found_tag.nil?
    Tag.create!(name: tag)
  end
end

# =============== Seed Categories ================================

categories = %w[
  Frontend
  Backend
]

for category in categories do
  found_category = Category.find_by(name: category)

  if found_category.nil?
    Category.create!(name: category)
  end
end
