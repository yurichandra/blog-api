class CreateAccessTokens < ActiveRecord::Migration[6.0]
  def change
    create_table :access_tokens do |t|
      t.string :access_token
      t.datetime :expired_at
      t.references :user, null: false, foreign_key: true
      t.timestamps
    end
  end
end
