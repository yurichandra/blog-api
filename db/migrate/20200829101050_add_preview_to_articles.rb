class AddPreviewToArticles < ActiveRecord::Migration[6.0]
  def change
    add_column :articles, :is_preview, :boolean
  end
end
