class CreateArticles < ActiveRecord::Migration[6.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :body
      t.string :slug
      t.string :cover_image
      t.integer :duration
      t.boolean :is_published
      t.belongs_to :category
      t.belongs_to :user

      t.timestamps
    end
  end
end
